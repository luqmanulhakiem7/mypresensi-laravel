<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    use HasFactory;
    protected $fillable = [
        'id', 'jamke', 'jamstart', 'jamend'
    ];

    /**
     * Get the presensi that owns the Jadwal
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    // public function presensi()
    // {
    //     return $this->belongsTo(Presensi::class);
    // }
}
