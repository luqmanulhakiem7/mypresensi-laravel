<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\str;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ApiLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:users',
            'level' => 'string|max:255',
            'password' => 'required|string|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'level' => "karyawan",
            'password' => Hash::make($request->password)
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'data' => $user,
            'access_token' => $token,
            'token_type' => 'Bearer'
        ]);
    }


    public function login(Request $request)
    {
        $email = $request->email;

        $user = User::where('email', request()->email)->first();
        if ($user) {
            if (Hash::check(request()->password, $user->password)) {
                Auth::attempt(['email' => $user->email, 'password' => request()->password]);
                if ($user->status != "Aktif") {
                    return response()->json([
                        'message' => 'akun anda terblokir silahkan hubungi admin'
                    ]);
                }else {
                    $token = $user->createToken('auth_token')->plainTextToken;
                    $user->login_attempt = 1;
                    $user->save();
                    return response()->json([
                        'user_id' => auth()->user()->id,
                        'name' => auth()->user()->name,
                        'alamat' => auth()->user()->alamat,
                        'hp' =>  auth()->user()->hp,
                        'ttl' => auth()->user()->ttl,
                        'email' => auth()->user()->email,
                        'password' => auth()->user()->password,
                        'status' => auth()->user()->status,
                        'pesan' => 'Login success',
                        'access_token' => $token,
                        'token_type' => 'Bearer'
                    ]);
                }
            }

            if ($user->login_attempt >= 5) {
                $user_id = User::select('id')->where('email', $email)->first();
                $updt = [
                    'status' => "Terblokir",

                ];
                $user_id->update($updt);
                return response()->json([
                    'message' => 'akun anda terblokir, Silahkan hubungi admin untuk membuka blokir.'
                ]);
            } else {
                $user->login_attempt = $user->login_attempt + 1;
                $user->save();
                return response()->json([
                    'message' => 'password anda salah'
                ]);
            }
        } else {
            return response()->json([
                'message' => 'Email tidak ditemukan'
            ]);
        };
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::user()->tokens()->delete();
        return response()->json([
            'message' => 'logout success'
        ]);
    }

    public function EditProfile(Request $request)
    {
        $uid = Auth::user()->id;
        $u = User::findOrFail($uid);
        $u->name = $request->input('name');
        $u->nik = $request->input('nik');
        $u->alamat = $request->input('alamat');
        $u->hp = $request->input('hp');
        $u->ttl = $request->input('ttl');
        $u->email = $request->input('email');
        $u->password = bcrypt($request->input('password'));

        $u->update();
        return response()->json([
            'message' => 'Data Berhasil Di Update'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
