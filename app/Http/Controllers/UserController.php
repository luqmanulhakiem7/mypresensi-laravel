<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \PDF;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword  = $request->keyword;

        $karyawan = User::where('name', 'LIKE', '%'.$keyword.'%')
        ->orWhere('email', 'LIKE', '%'.$keyword.'%')
        ->simplePaginate(10);
        
        
        return view('pages.user', compact('karyawan'));
    }
    public function cetak_pdf()
    {
        $karyawan = User::all();

        $pdf = PDF::loadview('pdf.data-user', ['karyawan' => $karyawan]);
        return $pdf->stream();
    }

    public function UserTambah()
    {
        return view('pages.user-add');
    }

    public function UserSimpan(Request $request)
    {
        // insert data ke table pegawai
        User::create([
            'name' => $request->name,
            'level' => $request->level,
            'email' => $request->email,
            'status'=>"Aktif",
            'password' => bcrypt($request->password),
            'remember_token' => Str::random(60),
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('user');
    }

    // method untuk edit data pegawai
    public function UserEdit($id)
    {
        // mengambil data pegawai berdasarkan id yang dipilih
        $karyawan = \DB::table('users')->where('id', $id)->get();
        // passing data pegawai yang didapat ke view edit.blade.php
        return view('pages.user-edit', compact('karyawan'));
    }
    
    // update data pegawai
    public function UserUpdate(Request $request)
    {
        // update data pegawai
        \DB::table('users')->where('id', $request->id)->update([
            'name' => $request->name,
            'nik' => $request->nik,
            'alamat' => $request->alamat,
            'hp' => $request->hp,
            'ttl' => $request->ttl,
            'level' => $request->level,
            'status' => $request->status,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('user');
    }
    // Edit User Active
    public function UserAktif()
    {
        return view('pages.user-set');
    }
    public function UserAktifUpdate(Request $request)
    {
        $this->validate($request, [
            'fotouser' => 'mimes:jpeg,png,,jpg|max:2048',
        ]);

        $fUser = $request->file('fotouser');

        $nama_fUser = time() . "_" . $fUser->getClientOriginalName();

        $lokasi_fUser = 'foto_user';
        $fUser->move($lokasi_fUser, $nama_fUser);

        $uid = Auth::user()->id;
        $u = User::findOrFail($uid);
        $u->name = $request->input('name');
        $u->fotouser = $nama_fUser;
        $u->nik = $request->input('nik');
        $u->alamat = $request->input('alamat');
        $u->hp = $request->input('hp');
        $u->ttl = $request->input('ttl');
        $u->email = $request->input('email');
        $u->password = bcrypt($request->input('password'));

        $u->update();
        return redirect('user-profile');

    }

    public function UserHapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        \DB::table('users')->where('id', $id)->delete();
        \DB::table('presensi')->where('user_id', $id)->delete();

        // alihkan halaman ke halaman pegawai
        return redirect('user');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = User::find($id);
        return view('pages.user-show', compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
