<?php

namespace App\Http\Controllers;

use App\Models\Jadwal;
use DateTime;
use DateTimeZone;
use App\Models\Presensi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \PDF;

class PresensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view('pages.absen');
    }

    public function keluar()
    {
        return view('pages.absen');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $timezone = 'Asia/Jakarta'; 
        $date = new DateTime('now', new DateTimeZone($timezone)); 
        $tanggal = $date->format('Y-m-d');
        $localtime = $date->format('H:i:s');
        $presensi = Presensi::where([
            ['user_id','=',auth()->user()->id],
            ['tgl','=',$tanggal],
        ])->first();
        if ($presensi){
            return redirect()->back()->with('alert', 'Anda Sudah Mengisi Absen Masuk');
        }else{
            Presensi::create([
                'user_id' => auth()->user()->id,
                'locm' => $request->locm,
                'tgl' => $tanggal,
                'jammasuk' => $localtime,
                'status' => 'Sedang Bekerja'
            ]);
            
            return redirect('absen');
        }

        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function halamanrekap(Request $request)
    {
        $keyword = $request->keyword;

        $presensi = Presensi::with('user')
        ->where('user_id', 'LIKE', '%'.$keyword.'%')
        ->latest()->simplePaginate(10);
        return view('pages.laporan-absen', compact('presensi'));
        // return view('pages.laporan-absen');
    }

   
    public function tampildatakeseluruhan($tglawal, $tglakhir)
    {
        $presensi = Presensi::with('user')->whereBetween('tgl',[$tglawal, $tglakhir])->orderBy('tgl','asc')->get();
        return view('pages.laporan-absen-all',compact('presensi'));
    }

    public function historyabsen()
    {
        $id = Auth::user()->id;
        $history = Presensi::where('user_id', $id )->latest()->take(10)->get();
        return view('pages.laporan-user', compact('history'));
    }

    public function cetak_pdf()
    {
        $presensi = Presensi::all();

        $pdf = PDF::loadview('pdf.data-absen-all', ['presensi' => $presensi]);
        return $pdf->stream();
    }
    public function cetak_pdf_tgl($tglawal, $tglakhir)
    {
        $presensi = Presensi::with('user')->whereBetween('tgl', [$tglawal, $tglakhir])->orderBy('tgl', 'asc')->get();

        $pdf = PDF::loadview('pdf.data-absen-all', ['presensi' => $presensi]);
        return $pdf->stream();
    }
   
    public function presensipulang(Request $request){
        $timezone = 'Asia/Jakarta'; 
        $date = new DateTime('now', new DateTimeZone($timezone)); 
        $tanggal = $date->format('Y-m-d');
        $localtime = $date->format('H:i:s');

        $presensi = Presensi::where([
            ['user_id','=',auth()->user()->id],
            ['tgl','=',$tanggal],
        ])->first();

       
        $dt=[
            'locp' => $request->locp,
            'status' => 'Hadir',
            'jamkeluar' => $localtime,
            'jamkerja' => date('H:i:s', strtotime($localtime) - strtotime($presensi->jammasuk))
        ];
        
        if ($presensi->jamkeluar == ""){
            $presensi->update($dt);
            return redirect('absen');
        }else{
            return redirect()->back()->with('alert', 'Anda Sudah Mengisi Absen Pulang');
        }
    }

    public function keterangan(){
        
    }
    

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Presensi::findorfail($id);
        $hapus->delete();

        return back();
    }
}
