<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function halamanlogin(){
        return view('Login.Login-aplikasi');
    }

    
    public function postlogin(Request $request){
        

        $email = $request->email;
        
        // if(Auth::attempt($request->only('email','password'))){
        //     return redirect('/dashboard');
        // }
        // elseif ($LogAttempts >= 3 ) {
        //     $user_id = User::select('id')->where('email', $email)->first();
        //     $updt = [
        //         'status' => 0,
        //     ];
        //     $user_id->update($updt);
        //     return redirect()->route('login')->with('error', 'kamu terblokir');
        // }else{
        //     count($LogAttempts)
        //     return redirect()->route('login')->with('error', 'email atau password anda salah');
        // }

        $user = User::where('email', request()->email)->first();
        if ($user) {
            if (Hash::check(request()->password, $user->password)) {
                Auth::attempt(['email' => $user->email, 'password' => request()->password]);
                $user->login_attempt = 1;
                $user->save();
                return redirect('/dashboard');
            }

            if ($user->login_attempt >= 5) {
                $user_id = User::select('id')->where('email', $email)->first();
                $updt = [
                    'status' => "Terblokir",
                ];
                $user_id->update($updt);
                return redirect()->route('login')->with('error', 'akun anda terblokir, Silahkan hubungi admin untuk membuka blokir.');
            } else {
                $user->login_attempt = $user->login_attempt + 1;
                $user->save();
                return redirect()->route('login')->with('error', 'password anda salah');
            }
        } else {
            return redirect()->route('login')->with('error', 'email anda salah');
        };
    }

    public function logout(){
        Auth::logout();
        return redirect ('/');
    }

    public function registrasi(){
        return view('Login.registrasi');
    }

    public function simpanregistrasi(Request $request){
        // dd($request->all());

        User::create([
            'name' => $request->name,
            'level' => $request->level,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'remember_token' => Str::random(60),
        ]);

        return view('welcome');

    }
}
