<?php

namespace App\Http\Controllers;
use App\Models\Presensi;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApiPresensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $presensi = Presensi::paginate (10);
        return response()->json([
            'data' => $presensi
        ]) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $timezone = 'Asia/Jakarta'; 
        $date = new DateTime('now', new DateTimeZone($timezone)); 
        $tanggal = $date->format('Y-m-d');
        $localtime = $date->format('H:i:s');
        $presensi = Presensi::where('user_id', auth()->user()->id)->orderBy('id', 'desc')->first();
        if ($presensi !== null && $presensi->tgl == $tanggal){
            return response()->json([
                'status' => 'loged'
            ]);
        }else{
            $now = Presensi::create([
                'user_id' => auth()->user()->id,
                'locm' => $request->locm,
                'tgl' => $tanggal,
                'jammasuk' => $localtime,
                'status' => 'Sedang Bekerja'
            ]);
            
            return response()->json([
                'data' => $now,
                'tgl sekarang' => $tanggal
            ]); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, presensi $presensi)
    {
        $timezone = 'Asia/Jakarta'; 
        $date = new DateTime('now', new DateTimeZone($timezone)); 
        $tanggal = $date->format('Y-m-d');
        $localtime = $date->format('H:i:s');
        $presensi = Presensi::where('user_id', auth()->user()->id)->orderBy('id', 'desc')->first();

        $validator = Validator::make($request->all(), [
            'locp'     => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        
        if ($presensi->jamkeluar == null && $presensi->tgl == $tanggal){
            $presensi->update([
                'locp' => $request->locp,
                'status' => 'Hadir',
                'jamkeluar' => $localtime,
                'jamkerja' => date('H:i:s', strtotime($localtime) - strtotime($presensi->jammasuk))
            ]);
            return response()->json([
                'data' => $presensi,
            ]); 

            return new PostResource(true, 'Data Post Berhasil Diubah!', $presensi);
        }else{
            return response()->json([
                'message' => 'sudah login'
            ]); 
        }
    }

    public function historyabsen()
    {
        $history = Presensi::where('user_id', auth()->user()->id)->latest()->take(10)->get();
        return response()->json([
            'meesage' => 'datauser',
            'data' => $history,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
