<?php

namespace Database\Seeders;

use App\Models\Jadwal;
use Illuminate\Database\Seeder;

class JadwalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jadwal::truncate();
        Jadwal::create([
            'jamke' => 'Masuk',
            'jamstart' => '08:00',
            'jamend' => '16:00',
        ]);
    }
}
