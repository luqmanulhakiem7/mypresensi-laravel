<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PresensiController;
use App\Http\Controllers\UserController;



Route::get('/', function () {
    return view('beranda');
});
Route::get('/panduanuser', function () {
    return view('/pages/panduanuser');
});
// Route::get('/user-setting', function () {
//     return view('pages.user-set');
// });
// Route::get('/form', function () {
//     return view('pages.form');
// });


// route::get('/registrasi',[LoginController::class,'registrasi'])->name('registrasi');
// route::post('/simpanregistrasi',[LoginController::class,'simpanregistrasi'])->name('simpanregistrasi');
route::get('/login',[LoginController::class,'halamanlogin'])->name('login');
route::post('/postlogin',[LoginController::class,'postlogin'])->name('postlogin');
route::get('/logout',[LoginController::class,'logout'])->name('logout');

Route::group(['middleware' => ['auth','ceklevel:admin,karyawan', "checkbanned:'Aktif'"]], function () {
    route::get('/dashboard',[HomeController::class,'index'])->name('home');

    Route::get('user-profile', [UserController::class, 'UserAktif'])->name('pages.user-set');
    Route::post('user-profile-update', [UserController::class, 'UserAktifUpdate'])->name('pages.user-set-update');
});

Route::group(['middleware' => ['auth','ceklevel:karyawan', "checkbanned:'Aktif'"]], function () {
    route::post('/simpan-masuk',[PresensiController::class,'store'])->name('simpan-masuk');
    route::get('/absen',[PresensiController::class,'index'])->name('absen');    
    // route::get('/absen',[PresensiController::class,'keluar'])->name('absen');   
    // route::get('keterangan',[PresensiController::class,'keterangan']);   
    Route::post('ubah-presensi',[PresensiController::class,'presensipulang'])->name('ubah-presensi');
    // Route::get('laporan',[PresensiController::class,'halamanrekap'])->name('filter-data'); 
    // Route::get('laporan/{tglawal}/{tglakhir}',[PresensiController::class,'tampildatakeseluruhan'])->name('filter-data-keseluruhan'); 
    Route::get('history',[PresensiController::class,'historyabsen']); 
});

Route::group(['middleware' => ['auth','ceklevel:admin', "checkbanned:'Aktif'"]], function () {
    // Laporan
    Route::get('laporan',[PresensiController::class,'halamanrekap'])->name('filter-data'); 
    Route::get('laporan-all/cetak_pdf', [PresensiController::class, 'cetak_pdf']);
    Route::get('laporan-hapus/{id}',[PresensiController::class,'destroy']); 
    Route::get('laporan/{tglawal}/{tglakhir}',[PresensiController::class,'tampildatakeseluruhan'])->name('filter-data-keseluruhan'); 
    Route::get('laporan-tgl/{tglawal}/{tglakhir}', [PresensiController::class, 'cetak_pdf_tgl']);
    Route::get('profile-user/{id}', [UserController::class, 'show']);
    
    // // Jadwal
    // Route::get('jadwal', [JadwalController::class, 'index']);
    // Route::get('jadwal-add', [JadwalController::class, 'create']);
    // Route::post('jadwal-simpan', [JadwalController::class, 'store']);
    // Route::get('edit-produk/{id}', [JadwalController::class, 'edit']);
    // Route::post('update-produk/{id}', [JadwalController::class, 'update']);
    // Route::get('jadwal/hapus/{id}', [JadwalController::class, 'destroy']);
    
    // User Controller
    Route::get('user', [UserController::class, 'index']);
    Route::get('user/cetak_pdf', [UserController::class, 'cetak_pdf']);
    Route::get('user-add', [UserController::class, 'UserTambah']);
    Route::post('user-simpan',[UserController::class, 'UserSimpan']);
    Route::get('user/edit/{id}',[UserController::class, 'UserEdit']);
    Route::post('user-update',[UserController::class, 'UserUpdate']);
    Route::get('user/hapus/{id}',[UserController::class, 'UserHapus']);
});


