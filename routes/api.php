<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ApiPresensiController;
use App\Http\Controllers\ApiLoginController;

// Route::group(['middleware => auth:sanctum'], function(){

// });
Route::post('/login', [ApiLoginController::class, 'login']);
Route::post('/register', [ApiLoginController::class, 'register']);
Route::get('/presensis', [ApiPresensiController::class, 'index']);


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum', "checkbanned:'Aktif'")->group(function () {
    Route::get('/history', [ApiPresensiController::class, 'historyabsen']);
    Route::get('/dprofil', [ApiController::class, 'showaktif']);
    Route::post('/logout', [ApiLoginController::class, 'logout']);
    Route::post('/profile', [ApiLoginController::class, 'EditProfile']);
    Route::post('/absenmasuk', [ApiPresensiController::class, 'store']);
    Route::post('/absenpulang', [ApiPresensiController::class, 'update']);
});


// route::get('/login', [LoginController::class, 'halamanlogin'])->name('login');
    // Route::apiResource('users', ApiController::class);
    // Route::apiResource('presensis', ApiPresensiController::class);