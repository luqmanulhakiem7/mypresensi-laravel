<!DOCTYPE html>
<html lang="en">
<head>

  @include('layouts.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed" onload="realtimeClock();initialize_map();initialize()">
<div class="wrapper">

  <!-- Preloader -->
    <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="{{URL::asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTELogo" height="60" width="60">
    </div>

  <!-- Navbar -->
    @include('layouts.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('layouts.sidebar')

  <!-- Content Wrapper. Contains page content -->
    @yield('content')
  <!-- /.content-wrapper -->
  
  {{-- Footer --}}
  @include('layouts.footer')
  @include('layouts.footer-control')
</div>
    <!-- ./wrapper -->

    {{-- script --}}
    @include('layouts.script')
</body>
</html>
