@extends('Home')

@section('content')
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        {{-- Start Allert --}}
            <div class="alert alert-dismissible bg-primary">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-check"></i> Selamat Datang {{ auth()->user()->name }}!</h5>
                  <hr>
                  Anda Telah Login Sebagai {{ auth()->user()->level }}.
                </div>
        <!-- Small boxes (Stat box) -->
        <div class="row">
          @if (auth()->user()->level == "admin")
          <div class="col-lg-3 col-6">
            <!-- small box -->
            {{-- <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>

                <p>Total User</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add  "></i>
              </div>
            </div> --}}
          </div>
          <!-- ./col -->
          @endif
          @if (auth()->user()->level == "karyawan")
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>Absen</h3>

                <p>Jangan lupa untuk absen ya </p>
              </div>
              <div class="icon">
                <i class="ion ion-calendar"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          @endif
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection