@extends('Home')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Laporan Absen</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/panduanuser">Panduan ?</a></li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="card">
              <div class="card-header">
                    <div class="float-right">
                         <form method="get" action="{{ url('laporan') }}">
                            <div class="row">
                                <input name="keyword" value="{{ request('keyword') }}" type="text" class="col-8 form-control"
                                placeholder="Cari User by id user">
                                <button type="submit" class="btn btn-primary mb-3 ml-2">Cari</button>
                            </div>
                        </form>
                    </div>
              </div>
               <div class="card-body">
                   
                <div class="row">
                    <div class="form-group mr-2">
                        <label for="label">Tanggal Awal</label>
                        <input type="date" name="tglawal" id="tglawal" class="form-control" />
                    </div>
                    <div class="form-group mr-2">
                        <label for="label">Tanggal Akhir</label>
                        <input type="date" name="tglakhir" id="tglakhir" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="">Lihat</label>
                        <a href="" onclick="this.href='/laporan/'+ document.getElementById('tglawal').value +
                            '/' + document.getElementById('tglakhir').value " class="btn btn-primary col-md-12">
                            Lihat</i>
                        </a>
                    </div>
                </div>
                 <div class="row">
                    <a href="laporan-all/cetak_pdf" target="_blank">
                        <button class="btn bg-pink">Download PDF</button>
                    </a>
                </div>
                {{-- /Laporan download --}}
                {{-- <div class="row">
                    <a href="">
                        <button>Excel</button>
                    </a>
                    <a href="">
                        <button>PDF</button>
                    </a>
                    <a href="">
                        <button>Print</button>
                    </a>
                </div> --}}
               </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered text-center">
                  <thead>
                    <tr>
                        <th>#</th>
                        <th>Tanggal</th>
                        <th>Nama Karyawan</th>
                        <th>Masuk</th>
                        <th>Keluar</th>
                        <th>Lokasi</th>
                        <th>Status</th>
                        <th>Jumlah Jam Kerja</th>
                        <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- <tr>
                        <td colspan="5" class="text-center">
                            Data belum ada silahkan pilih tanggal dulu
                        </td>
                    </tr> --}}
                    <?php 
                    $no = 1
                    ?>
                    @foreach ($presensi as $item)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $item->tgl }}</td>
                        <td>{{ $item->user->name }}</td>
                        <td>{{ $item->jammasuk }}</td>
                        <td>{{ $item->jamkeluar }}</td>
                        <td>
                            @if ($item->locm < 0)
                                <a href="http://maps.google.com/maps?q=loc:{{ $item->locm }}">
                                <button class="btn btn-sm btn-primary">
                                <i class="fas fa-map-marker"></i>
                                Masuk
                                </button>
                                </a>
                            @else
                                <a href="#">
                                    <button class="btn btn-sm btn-danger" disabled>
                                    <i class="fas fa-map-marker"></i>
                                    Masuk
                                    </button>
                                    </a>
                            @endif
                            {{-- Pulang --}}
                            @if ($item->locp < 0)
                                <a href="http://maps.google.com/maps?q=loc:{{ $item->locp }}">
                                <button class="btn btn-sm btn-primary">
                                <i class="fas fa-map-marker"></i>
                                Pulang
                                </button>
                                </a>
                            @else
                                <a href="#">
                                    <button class="btn btn-sm btn-danger" disabled>
                                    <i class="fas fa-map-marker"></i>
                                    Pulang
                                    </button>
                                    </a>
                                
                            @endif
                        </td>
                        <td>{{ $item->status }}</td>
                        <td>{{ $item->jamkerja }}</td>
                        <td>
                            <a href="{{ 'laporan-hapus/'. $item->id }}">Hapus</a>
                            ||
                            <a href="{{ 'profile-user/'. $item->user->id }}">Info User</a>
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="mt-3">
                    {{ $presensi->links() }}
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                
              </div>
            </div>
            <!-- /.card -->
        <!-- /.content -->
    </div>
@endsection