@extends('Home')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Panduan</h1>
            </div>
            <div class="col-sm-6">
            </div>
            </div>
        </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Panduan CRUD User</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
                </button>
            </div>
            </div>
            <div class="card-body">
                <br>
            <h3>Membuat User Baru</h3>
            <p>1. Klik tombol <span>Buat User</span></p>
            <img src="{{URL::asset('dist/img/BuatUser.GIF')}}" alt="Gambar Tombol Buat User" width="400px" height="250px">
            <br><br>
            <p>2. Isi Data jika data sudah diisi klik tombol <span>submit</span></p>
            <img src="{{URL::asset('dist/img/BuatUserSubmit.GIF')}}" alt="Gambar Submit User" width="400px" height="250px">
            <br><br><br><br>
            
            <h3>Mengedit User</h3>
            <p>1. Pilih User mana yang akan di edit lalu klik Edit</p>
            <img src="{{URL::asset('dist/img/EditUser.GIF')}}" alt="Gambar Tombol EditUser" width="400px" height="250px">
            <br><br>
            <p>2. isi data, jika sudah klik tombol submit</p>
            <img src="{{URL::asset('dist/img/EditUserSubmit.GIF')}}" alt="Gambar Submit EditUser" width="400px" height="250px">
            <br><br><br><br>
            
            <h3>Delete User</h3>
            <p>1. Pilih user yang akan dihapus lalu klik tombol delete</p>
            <img src="{{URL::asset('dist/img/DeleteUser.GIF')}}" alt="Gambar Delete User" width="400px" height="250px">
            <br><br><br><br>
            </div>
            <!-- /.card-body -->
            <!-- /.card-footer-->
        </div>
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Panduan Melihat Laporan Berdasarkan Tanggal</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
                </button>
            </div>
            </div>
            <div class="card-body">
                <br>
            <p>1. Pilih Tanggal Awal</p>
            <p>2. Pilih Tanggal Akhir</p>
            <p>3. Klik tombol Lihat</p>
            <img src="{{URL::asset('dist/img/SelectTgl.GIF')}}" alt="Gambar Pilih laporan berdasarkan tgl" width="400px" height="250px">
            <br><br>
            
            </div>
            <!-- /.card-body -->
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>   
@endsection