@extends('Home')

@section('content')




<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Absen</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">Absen</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        {{-- <div class="row justify-content-center">
          <video src="" id="preview"></video>
        </div>
        <button id="capture">Capture</button>
        <canvas id="output" style="display: none"></canvas> --}}
        {{-- <img id="result"> --}}
        <div class="row justify-content-around pl-5 pr-5 ">
          <div class="isi">
            {{-- <div id="title">Show Position In Map</div> --}}
            <div id="current" class="text-center">Initializing...</div>
            <div id="map_canvas" style="width:400px; height:500px"></div>
          </div>
          <div class="isi pt-5">
            <div class="isi-1">
              <div class="small-box bg-info ">
                <div class="inner">
                  <label id="clock" style="font-size: 100px; color: #ffff; -webkit-text-stroke: 3px #000;
                              text-shadow: 4px 4px 10px #000,
                              ">
                  </label>
                </div>
              </div>
            </div>
            <div class="isi-2">
              <div class="row justify-content-center">
                <div class="masuk">
                   <form action="{{ route('simpan-masuk') }}" method="post">
                      {{ csrf_field() }}
                      <div class="form-group">
                        <input type="hidden" id="clock">
                        <input type="hidden" id="locm" name="locm">
                      </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-primary mr-2">Absen Masuk</button>
                          </div>
                  </form>
                </div>
                <div class="keluar">
                  <form action="{{ route('ubah-presensi') }}" method="post">
                      {{ csrf_field() }}
                      <div class="form-group">
                              <input type="hidden" id="clock">
                              <input type="hidden" id="locp" name="locp">
                      </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-primary ml-2">Absen Pulang</button>
                          </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="row justify-content-center">
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
      alert(msg);
    }
  </script>
@endsection