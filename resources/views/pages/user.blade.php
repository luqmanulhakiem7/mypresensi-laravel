@extends('Home')

@section('content')
	<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Data User</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/panduanuser">Panduan ?</a></li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="card">
            <div class="card-header">
                <div class="row float-right">
                    <a href="/user-add">
						<button class="btn btn-primary">Buat User</button>
					</a>
                </div>
              </div>
               <div class="card-body">
                    <div class="row-float-left">
                        <h4>Filter</h4>
                        <form method="get" action="{{ url('user') }}">
                            <div class="row">
                                <input name="keyword" value="{{ request('keyword') }}" type="text" class="col-3 form-control"
                                placeholder="Cari User">
                                <button type="submit" class="btn btn-primary mb-3 ml-2">Cari</button>
                            </div>
                        </form>
                        <a href="user/cetak_pdf" target="_blank">
                            <button class="btn bg-pink">Download PDF</button>
                        </a>
                    </div>
               </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead class="text-center">
                    <tr>
						<th>#</th>
                        <th>Nama Karyawan</th>
                        <th>Roles</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
					@foreach($karyawan as $p)
						<tr>
							<td class="text-center">{{ $no++ }}</td>
							<td>{{ $p->name }}</td>
							<td>{{ $p->level }}</td>
							<td>{{ $p->email }}</td>
							<td>
                                @if ($p->status == 'Aktif')
                                    <button class="btn btn-sm bg-green">{{ $p->status }}</button>
                                @endif
                                @if ($p->status == 'Terblokir')
                                    <button class="btn btn-sm btn-danger">{{ $p->status }}</button>
                                @endif
                            </td>
							<td class="text-center">
								<a href="/user/edit/{{ $p->id }}">
								<i class="fa fa-pen"></i>

									Edit</a>
								|
								<a href="/user/hapus/{{ $p->id }}">
								<i class="fa fa-trash"></i>
									Hapus
								</a>
							</td>
						</tr>
					@endforeach
                    {{-- @foreach ($presensi as $item)
                    <tr>
                        <td>{{ $item->user->name }}</td>
                        <td>{{ $item->tgl }}</td>
                        <td>{{ $item->jammasuk }}</td>
                        <td>{{ $item->jamkeluar }}</td>
                        <td>{{ $item->jamkerja }}</td>
                    </tr>
                    @endforeach --}}
                  </tbody>
                </table>
                 <div class="mt-3">
                    {{ $karyawan->links() }}
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                
              </div>
            </div>
            <!-- /.card -->
        <!-- /.content -->
    </div>		
	
@endsection
	
		{{-- <body>
			<h2>www.malasngoding.com</h2>
			<h3>Data karyawan</h3>
		
			<a href="/user-add"> + Tambah karyawan Baru</a>
			
			<br/>
			<br/>
		
			<table border="1">
				<tr>
					<th>Nama</th>
					<th>Roles</th>
					<th>Email</th>
					<th>Opsi</th>
				</tr>
				@foreach($karyawan as $p)
				<tr>
					<td>{{ $p->name }}</td>
					<td>{{ $p->level }}</td>
					<td>{{ $p->email }}</td>
					<td>
						<a href="/user/edit/{{ $p->id }}">Edit</a>
						|
						<a href="/user/hapus/{{ $p->id }}">Hapus</a>
					</td>
				</tr>
				@endforeach
			</table>
		
		
		</body> --}}


