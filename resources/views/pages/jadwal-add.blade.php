@extends('Home')

@section('content')
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Jadwal</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah Jadwal</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Isi Data Dibawah Ini</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/jadwal-simpan" method="post">
		        {{ csrf_field() }}

                <div class="card-body">
                    <div class="form-group">
                        <label >Jam Ke    </label>
                        <input type="text" class="form-control" name="jamke" placeholder="Masukkan Nama Anda" required>
                    </div>
                    <div class="form-group">
                        <label for="jamstart">Jam Masuk:</label>

                        <input type="time" id="jamstart" name="jamstart"
                            class="form-control col-2" required>

                        <small>Jam kerja biasanya dari 8am sampai 16pm</small>
                    </div>
                    <div class="form-group">
                        <label for="jamend">Jam Pulang:</label>

                        <input type="time" id="jamend" name="jamend"
                            class="form-control col-2" required>

                        <small>Jam kerja biasanya dari 8am sampai 16pm</small>
                    </div>
                    
                  {{-- File Input --}}
                  {{-- <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                  </div> --}}
                  {{-- <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                  </div> --}}
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" value="Simpan Data">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection