@extends('Home')

@section('content')
	<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Data Jadwal</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item active">Jadwal</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="card">
              <div class="card-header">
              </div>
               <div class="card-body">
                <div class="row float-right">
                    <a href="/jadwal-add">
						<button class="btn btn-primary">Buat Jadwal</button>
					</a>
                </div>
               </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead class="text-center">
                    <tr>
						<th>#</th>
                        {{-- <th>Hari</th> --}}
                        <th>Jam Ke</th>
                        <th>Jam Masuk</th>
                        <th>Jam Pulang</th>
                        <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
					@foreach($jadwal as $p)
						<tr>
							<td class="text-center">{{ $no++ }}</td>
							<td>{{ $p->jamke }}</td>
							<td>{{ $p->jamstart }}</td>
							<td>{{ $p->jamend }}</td>
							<td class="text-center">
								<a href="/Jadwal/edit/{{ $p->id }}">
								<i class="fa fa-pen"></i>

									Edit</a>
								|
								<a href="/jadwal/hapus/{{ $p->id }}">
								<i class="fa fa-trash"></i>
									Hapus
								</a>
							</td>
						</tr>
					@endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                
              </div>
            </div>
            <!-- /.card -->
        <!-- /.content -->
    </div>		
	
@endsection