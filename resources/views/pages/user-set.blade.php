@extends('Home')

@section('content')
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">My Profile</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{ url('user-profile-update') }}" method="post" enctype="multipart/form-data">
		        {{ csrf_field() }}

                <div class="card-body">
		            {{-- <input type="hidden" name="id" value="{{ Auth::user()->id }}"> <br/> --}}
                    <div class="form-group">
                      <div class="col">
                        <img src="{{ url('/foto_user/'. Auth::user()->fotouser ) }}" alt="Foto User" width="200" height="200">
                      </div>
                      <div class="col mt-2">
                        <label for="">Ganti Foto : </label>
                        <input type="file" name="fotouser" value="{{ Auth::user()->fotouser }}">
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Lengkap</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="name" value="{{ Auth::user()->name }}" placeholder="Masukkan Nama Anda">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">NIK</label>
                        <input type="text" class="form-control" name="nik" value="{{ Auth::user()->nik }}" placeholder="Masukkan No NIK Anda">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Alamat</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="alamat" value="{{ Auth::user()->alamat }}" placeholder="Masukkan Alamat Anda">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">No. Hp</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="hp" value="{{ Auth::user()->hp }}" placeholder="Masukkan No Hp Anda">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tempat & Tanggal Lahir</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="ttl" value="{{ Auth::user()->ttl }}" placeholder="Kota, Tanggal Bulan Tahun">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" name="email" value="{{ Auth::user()->email }}" placeholder="Masukkan Email Anda">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Masukkan Password Anda">
                    </div>
                  {{-- File Input --}}
                  {{-- <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                  </div> --}}
                  {{-- <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                  </div> --}}
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" value="Simpan Data">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection