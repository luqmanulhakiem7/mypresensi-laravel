@extends('Home')

@section('content')
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Info User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Info User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Profile User</h3>
              </div>
              <!-- /.card-header -->

                <div class="card-body row">
                    <div class="col-4 pl-5">
                      <img src="{{ url('/foto_user/'.$show->fotouser) }}" alt="" width="210" height="210">
                    </div>
                    <div class="col-8">
                                            <div class="form-group">
                        <label for="exampleInputEmail1">Nama    </label>
                        <p>{{$show->name}}</p>
                    </div>
                                            <div class="form-group">
                        <label for="exampleInputEmail1">Email    </label>
                        <p>{{$show->email}}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Alamat :</label>
                        @if ($show->alamat == "")
                            <p>Alamat Belum di setting</p>
                        @else
                            <p>{{$show->alamat}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">No Hp :</label>
                        @if ($show->hp == "")
                            <p>Nomer Belum di setting</p>
                        @else
                            <p>{{$show->hp}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tempat & Tanggal Lahir :</label>
                        @if ($show->ttl == "")
                            <p>Tempat tanggal lahir Belum di setting</p>
                        @else
                            <p>{{$show->ttl}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleSelectBorder">Role</label>
                        @if ($show->level == "karyawan")
                            <p>
                                <button class="btn btn-sm btn-primary">{{$show->level}}</button>
                            </p>
                        @endif
                        @if ($show->level == "admin")
                            <p>
                                <button class="btn btn-sm bg-dark">{{$show->level}}</button>
                            </p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleSelectBorder">Status Akun</label>
                        @if ($show->status == "Aktif")
                            <p>
                                <button class="btn btn-sm bg-green">{{$show->status}}</button>
                            </p>
                        @endif
                        @if ($show->status == "Terblokir")
                            <p>
                                <button class="btn btn-sm bg-danger">{{$show->status}}</button>
                            </p>
                        @endif
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection