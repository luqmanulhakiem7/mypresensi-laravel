@extends('Home')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">History Absen</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="card">
              <div class="card-header">
              </div>
               <div class="card-body">
                {{-- /Laporan download --}}
                {{-- <div class="row">
                    <a href="">
                        <button>Excel</button>
                    </a>
                    <a href="">
                        <button>PDF</button>
                    </a>
                    <a href="">
                        <button>Print</button>
                    </a>
                </div> --}}
               </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered text-center">
                  <thead>
                    <tr>
                        <th>#</th>
                        <th>Tanggal</th>
                        <th>Nama Karyawan</th>
                        <th>Masuk</th>
                        <th>Keluar</th>
                        <th>Lokasi</th>
                        <th>Status</th>
                        <th>Jumlah Jam Kerja</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- <tr>
                        <td colspan="5" class="text-center">
                            Data belum ada silahkan pilih tanggal dulu
                        </td>
                    </tr> --}}
                    <?php 
                    $no = 1
                    ?>
                    @foreach ($history as $item)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $item->tgl }}</td>
                        <td>{{ $item->user->name }}</td>
                        <td>{{ $item->jammasuk }}</td>
                        <td>{{ $item->jamkeluar }}</td>
                        <td>
                            @if ($item->locm < 0)
                                <a href="http://maps.google.com/maps?q=loc:{{ $item->locm }}">
                                <button class="btn btn-sm btn-primary">
                                <i class="fas fa-map-marker"></i>
                                Masuk
                                </button>
                                </a>
                            @else
                                <a href="#">
                                    <button class="btn btn-sm btn-danger" disabled>
                                    <i class="fas fa-map-marker"></i>
                                    Masuk
                                    </button>
                                    </a>
                            @endif
                            {{-- Pulang --}}
                            @if ($item->locp < 0)
                                <a href="http://maps.google.com/maps?q=loc:{{ $item->locp }}">
                                <button class="btn btn-sm btn-primary">
                                <i class="fas fa-map-marker"></i>
                                Pulang
                                </button>
                                </a>
                            @else
                                <a href="#">
                                    <button class="btn btn-sm btn-danger" disabled>
                                    <i class="fas fa-map-marker"></i>
                                    Pulang
                                    </button>
                                    </a>
                                
                            @endif
                        </td>
                        <td>{{ $item->status }}</td>
                        <td>{{ $item->jamkerja }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="mt-3">
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                
              </div>
            </div>
            <!-- /.card -->
        <!-- /.content -->
    </div>
@endsection