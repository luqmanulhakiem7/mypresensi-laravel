
<!-- jQuery -->
	<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
  <!-- jQuery UI 1.11.4 -->
	<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
{{-- Location --}}
<script src="{{ asset('js/geo.js') }}"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script>
  
    function initialize_map()
      {
        var myOptions = {
            zoom: 4,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP      
          }	
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
      }
    function initialize()
      {
        if(geo_position_js.init())
        {
          document.getElementById('current').innerHTML="Receiving...";
          geo_position_js.getCurrentPosition(show_position,function(){document.getElementById('current').innerHTML="Couldn't get location"},{enableHighAccuracy:true});
        }
        else
        {
          document.getElementById('current').innerHTML="Functionality not available";
        }
      }

    function show_position(p)
    {
      document.getElementById('current').innerHTML="latitude="+p.coords.latitude+" longitude="+p.coords.longitude;
      var pos=new google.maps.LatLng(p.coords.latitude,p.coords.longitude);
      var ltt = p.coords.latitude;
      var lgt = p.coords.longitude;
      var posall = ltt+','+lgt;
      document.getElementById('locm').value = posall;
      document.getElementById('locp').value = posall;
      map.setCenter(pos);
      map.setZoom(14);

      var infowindow = new google.maps.InfoWindow({
          content: "<strong>yes</strong>"
      });

      var marker = new google.maps.Marker({
          position: pos,
          map: map,
          title:"Kamu Disini"
      });

      google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
      });
      
    }
    </script>
    {{-- 24 Hours time --}}
    <script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>

<!-- AdminLTE for demo purposes -->
<!-- <script src="dist/js/demo.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="dist/js/pages/dashboard.js"></script> -->